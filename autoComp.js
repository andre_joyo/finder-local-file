var index=0;
var countries_info=[];
var countries_names=[];
var first_letter_countries=[];
var coincidences=[];
function storage(country_info) {
    countries_info[index]=country_info;
    countries_names[index]=country_info[3];
    index++;
    localStorage.setItem("country"+index, country_info);
}
function show_coincidences(text) {
    length=text.value.length;
    filter_first_letter(text.value.charAt(0));
    filter_names_countries(text.value);
    coincidences_div=document.getElementById("coincidences_div");
    while (coincidences_div.firstChild) {
        coincidences_div.removeChild(coincidences_div.firstChild);
    }
    for (let i = 0; i < coincidences.length; i++) {
        var coincidence_div = document.createElement("div");
        var coincidence_content = document.createTextNode(coincidences[i]); 
        coincidence_div.appendChild(coincidence_content);  
        coincidences_div.appendChild(coincidence_div);
        coincidence_div.id = "coincidence"+i; 
        coincidence_div.className = "coincidence";
        coincidence_div.setAttribute("country_option", coincidence_content.textContent);
        coincidences_div.addEventListener("click", function (e) {
            var input_text=document.getElementById("input_country");
            input_text.value=e.target.getAttribute("country_option");  
            while (coincidences_div.firstChild) {
                coincidences_div.removeChild(coincidences_div.firstChild);
            }
        })    
    }
}
function filter_first_letter(first_letter) {
    while (first_letter_countries.length > 0) {
        first_letter_countries.pop(); 
    }
    first_letter=first_letter.toUpperCase();
    var j=0;
    for (let i = 0; i < countries_names.length; i++) {
        if (first_letter==countries_names[i].charAt(0)){
            first_letter_countries[j]=countries_names[i];
            j++;
        }
    }
}
function filter_names_countries(inserted_text) {
    var len = inserted_text.length;
    var j=0;
    while (coincidences.length > 0) {
        coincidences.pop(); 
    }
    for (let i = 0; i < first_letter_countries.length; i++) {
        if(inserted_text.substr(1)==first_letter_countries[i].substr(1, len-1)){
            coincidences[j]=first_letter_countries[i];
            j++;
        }
    }
}