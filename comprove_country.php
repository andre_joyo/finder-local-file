<?php
    $country=$_POST["country"];
    //Variable donde recogemos el resultado de la consulta
    $json_array=array();
    $no_json_array=array();
    //Respuesta
    $response=false;
    $file = fopen("countries.csv", "r+");
    $index_country_searched_count=0;
  	//Llenar arreglo actualizando contador
    while (($datos = fgetcsv($file, ";")) == true) {
        //Incrementar contador
        if ($country==$datos[3]) {
        	$datos[4]=$datos[4]+1;
            $response=true;
            $index_country_searched=$index_country_searched_count;
        }
        array_push($json_array, json_encode($datos));
        array_push($no_json_array, $datos);
        $index_country_searched_count++;
    }
    fclose($file);
    //Eliminar csv actual
    unlink('countries.csv');
    //Crear csv con $json_array
    $file = fopen("countries.csv", "w");
    for ($i=0; $i < count($no_json_array); $i++) { 
        for ($j=0; $j < 4; $j++) { 
            fwrite($file, $no_json_array[$i][$j] .",");            # code...
        }
        fwrite($file, $no_json_array[$i][4] .PHP_EOL);
    } 
    //Mostramos el resultado. Este 'echo' será el que recibirá la conexión AJAX
	if ($response==true) {
        echo $json_array[$index_country_searched];
    } else{
        echo "ERROR 404";
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
    }
    fclose($file);
?>
