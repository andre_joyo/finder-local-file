<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>COUNTRY SEARCHER</title>
    <script src="autoComp.js"></script>
    <link rel="stylesheet" href="style.css">
    <script src="search_country.js"></script>
</head>
<body id="body" > <!--onload="thisf()"-->
    <?php
        require_once ("header.php"); //Es un requisito mínimo ->require
    ?>    
        <form name="formulario" action=""  onSubmit="enviarDatos(); return false" >
            <input type="search" name="country" id="input_country" onkeyup="show_coincidences(this)">
            <div id="coincidences_div"></div>
            <input id="button" type="submit" value="Search">
        </form>
     <?php   
        require  ("info_country.php");
        require ("get_info.php");
        require_once ("footer.php"); //Es un requisito mínimo ->require
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIRHK539QH-MM7UIZzNyDt0O0A-vHeRhc&callback=initMap"
    async defer></script>
</body>
</html>