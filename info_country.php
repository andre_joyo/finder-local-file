<br>
<section>
    <div id="map">Mapa</div>
    <div id="info">
        <div id="name" class="property">
            <label for="name">Country</label>
        </div>
        <div id="code" class="property">
            <label for="code">Country code</label>
        </div>
        <div id="latitude" class="property">
            <label for="latitude">Latitude</label>
        </div>
        <div id="longitude" class="property">
            <label for="longitude">Longitude</label>
        </div>
    </div>
</section>