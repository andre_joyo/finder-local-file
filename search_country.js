var array=["SP","40.463","-3.749","Spain"];
function initMap() {
    lati=parseInt(array[1].substr(0,6));
    longi=parseInt(array[2].substr(0,6));
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lati, lng: longi},
        zoom: 8
    });
}
var array_data=[];
  function enviarDatos(){
    array_data[0]=document.getElementById("code");
    array_data[1]=document.getElementById("latitude");
    array_data[2]=document.getElementById("longitude");
    array_data[3]=document.getElementById("name");
    var input_text=document.getElementById("input_country");
    var index_country=0;

    //Recogemos los valores introducidos en los campos de texto
    country = document.formulario.country.value;

    //Aquí será donde se mostrará el resultado

    //instanciamos el objetoAjax
    ajax = new XMLHttpRequest();

    //Abrimos una conexión AJAX pasando como parámetros el método de envío, y el archivo que realizará las operaciones deseadas
    ajax.open("POST", "comprove_country.php", true);

    //cuando el objeto XMLHttpRequest cambia de estado, la función se inicia
    ajax.onreadystatechange = function() {
        //Cuando se completa la petición, mostrará los resultados
          if (ajax.readyState == 4){
              //El método responseText() contiene el texto de nuestro 'consultar.php'. Por ejemplo, cualquier texto que mostremos por un 'echo'
              if (ajax.responseText=="ERROR 404"){
                  alert("Páis no encontrado, ingrese otro nombre.")
              } else{
                    resp = (ajax.responseText)
                    resp=resp.replace("[", "");
                    resp=resp.replace("]", "");
                    resp=resp.replace('"', "");
                    resp=resp.replace('"', "");
                    resp=resp.replace('"', "");
                    resp=resp.replace('"', "");
                    resp=resp.replace('"', "");
                    resp=resp.replace('"', "");
                    resp=resp.replace('"', "");
                    resp=resp.replace('"', "");
                    array = resp.split(",");
                    for (let i = 0; i < 4; i++) {
                        var t=document.createTextNode(" "+array[i]);
                        array_data[i].removeChild(array_data[i].lastChild);
                        array_data[i].appendChild(t);
                    }
                    initMap();
                }
        }
    }
  //Llamamos al método setRequestHeader indicando que los datos a enviarse están codificados como un formulario.
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");

  //enviamos las variables a 'consulta.php'
  ajax.send("&country="+country);
}